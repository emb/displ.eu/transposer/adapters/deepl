cd
mkdir deepl
cd deepl
git clone https://git.fairkom.net/emb/displ.eu/transposer/service-connector
git clone https://git.fairkom.net/emb/displ.eu/transposer/shared-lib.git
cd shared-lib
cd modules
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/utils
git clone https://git.fairkom.net/emb/displ.eu/transposer/modules/adapter-utils
cd ../adapters
git clone https://git.fairkom.net/emb/displ.eu/transposer/adapters/deepl.git
cd ../../service-connector
python3 -m venv ./venv
source venv/bin/activate
pip install -r requirements.txt
pip install -r ../shared-lib/modules/utils/requirements.txt
pip install -r ../shared-lib/modules/adapter-utils/requirements.txt
pip install -r ../shared-lib/adapters/deepl/requirements.txt
